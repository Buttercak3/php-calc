<?php
namespace Calc;

class Operator implements IStackFunction
{
    const ASSOC_LEFT = 1;
    const ASSOC_RIGHT = 2;

    /** @var string */
    private $symbol;

    /** @var int */
    private $params;

    /** @var int */
    private $assoc;

    /** @var int */
    private $precedence;

    /** @var callable */
    private $handler;

    public function __construct(
        $symbol,
        $assoc,
        $precedence,
        callable $handler,
        $params = null
    ) {
        $this->symbol = $symbol;
        $this->assoc = $assoc;
        $this->precedence = $precedence;
        $this->handler = $handler;

        if ($params !== null) {
            $this->params = $params;
        } else {
            $function = new \ReflectionFunction($handler);
            $this->params = $function->getNumberOfRequiredParameters();
        }
    }

    public function execute($args)
    {
        return call_user_func_array($this->handler, $args);
    }

    public function applyToStack(array & $stack)
    {
        $args = $this->params > 0 ? array_splice($stack, -$this->params) : [];
        $result = $this->execute($args);
        array_push($stack, $result);
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getAssociativity()
    {
        return $this->assoc;
    }

    public function getPrecedence()
    {
        return $this->precedence;
    }
}
