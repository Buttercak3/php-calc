<?php
namespace Calc;

/**
 * Contains a static method to tokenize a string.
 */
class Tokenizer
{
    /**
     * Splits a string up into numeric, alphanumeric and symbolic tokens.
     *
     * @param string $str
     * @return array
     */
    public static function parse($str)
    {
        $matches = [];

        // This regular expression can be split up into 4 main parts, connected
        // with ORs ("|").
        // The first part (-?\d+(\.\d*)?) captures numbers with an optional
        // minus sign and optional decimals.
        // The second part ([a-z][a-z0-9_]+) captures names consisting of
        // numbers, letters and underscores, but always starting with letters.
        // The third part ([()]) captures brackets.
        // The fourth part ([^a-z0-9\s]+) captures everything that consists of
        // symbols other than numbers, letters and whitespace.
        if (preg_match_all("/(-?\d+(\.\d+)?)|([a-z][a-z0-9_]*)|([()])|([^a-z0-9\s]+)/i", $str, $matches)) {
            return $matches[0];
        } else {
            throw new ParseError("Tokenization failed");
        }
    }
}
