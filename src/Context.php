<?php
namespace Calc;

/**
 * Provides context for mathematical formulas.
 *
 * Keeps track of variables, operators and functions that can be used in calculations.
 */
class Context
{
    /** @var array */
    private $functions = [];

    /** @var array */
    private $operators = [];

    /** @var array */
    private $vars = [];

    /** @var self */
    private static $defaultContext;

    /**
     * Returns a Context with standard operators and functions like +, -, *, /, cos, sin, ...
     *
     * @return static
     */
    public static function getDefault()
    {
        if (static::$defaultContext === null) {
            static::$defaultContext = static::createDefault();
        }

        return static::$defaultContext;
    }

    protected function createDefault()
    {
        $context = new self();

        $context->addOperator("+", Operator::ASSOC_LEFT, 2, function ($a, $b) {
            return $a + $b;
        });

        $context->addOperator("-", Operator::ASSOC_LEFT, 2, function ($a, $b) {
            return $a - $b;
        });

        $context->addOperator("*", Operator::ASSOC_LEFT, 3, function ($a, $b) {
            return $a * $b;
        });

        $context->addOperator("/", Operator::ASSOC_LEFT, 3, function ($a, $b) {
            return $a / $b;
        });

        $context->addFunction("cos", function ($x) {
            return cos($x);
        });

        $context->addFunction("sin", function ($x) {
            return sin($x);
        });

        $context->setVar("pi", M_PI);
        $context->setVar("e", M_EULER);

        return $context;
    }

    /**
     * Creates a new context with the functions, operators and variables of another.
     *
     * @params Context $context
     * @return static
     */
    public static function clone(Context $context)
    {
        $new = new static();
        $new->import($context);

        return $new;
    }

    public function __construct()
    {

    }

    public function import(Context $other)
    {
        $this->functions = $other->functions;
        $this->operators = $other->operators;
        $this->vars = $other->vars;
    }

    /**
     * Returns true if a symbol is a known operator to this Context.
     *
     * @param string $symbol
     * @return bool
     */
    public function isOperator($symbol)
    {
        return isset($this->operators[$symbol]);
    }

    /**
     * Returns the operator with the specified symbol.
     *
     * @param string $symbol
     * @return Operator
     */
    public function getOperator($symbol)
    {
        return $this->operators[$symbol];
    }

    /**
     * Registers a new operator in this context. After an operator has been added,
     * it can be used in calculations.
     *
     * @param string $symbol The symbol to be used in formulas
     * @param int $assoc The operator's associativity; Either Operator::ASSOC_LEFT or Operator::ASSOC_RIGHT
     * @param int $precedence The operator's priority over other operators
     * @param callable $handler A callback function that resolves the operation
     * @param int $params The number of parameters the operator takes
     * @return void
     */
    public function addOperator(
        $symbol,
        $assoc,
        $precedence,
        callable $handler,
        $params = null
    ) {
        $this->operators[$symbol] = new Operator(
            $symbol,
            $assoc,
            $precedence,
            $handler,
            $params
        );
    }

    /**
     * Returns true if $name is a function known to this context.
     *
     * @param string $name
     * @return bool
     */
    public function isFunction($name)
    {
        $name = strtolower($name);

        return isset($this->functions[$name]);
    }

    /**
     * Returns the function with the given name.
     *
     * @param string $name
     * @return Func
     */
    public function getFunction($name)
    {
        $name = strtolower($name);

        return $this->functions[$name];
    }

    /**
     * Registers a new function in this context. After a function has been added,
     * it can be used in calculations.
     *
     * @param string $name The name of the function to be used in formulas
     * @param callable $handler A callback function that resolves the operation
     * @param int $params The number of parameters the function takes
     * @return void
     */
    public function addFunction($name, callable $handler, $params = null)
    {
        $name = strtolower($name);

        $this->functions[$name] = new Func($name, $handler, $params);
    }

    /**
     * Returns true is $name is a variable known to this context.
     *
     * @param string $name
     * @return bool
     */
    public function isVar($name)
    {
        $name = strtolower($name);

        return isset($this->vars[$name]);
    }

    /**
     * Returns the value of a variable.
     *
     * @param string $name
     * @return float
     */
    public function getVar($name)
    {
        $name = strtolower($name);

        return $this->vars[$name];
    }

    /**
     * Sets the value of a variable.
     *
     * @param string $name
     * @param float $value
     * @return void
     */
    public function setVar($name, $value)
    {
        $name = strtolower($name);

        $this->vars[$name] = $value;
    }
}
