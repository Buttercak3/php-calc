<?php
namespace Calc;

function is_identifier($str)
{
    return preg_match("/^[a-z][a-z0-9_]*$/", $str) === 1;
}

function array_peek($arr, $offset = 0)
{
    return $arr[count($arr) - 1 - $offset];
}
