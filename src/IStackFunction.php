<?php
namespace Calc;

interface IStackFunction
{
    public function applyToStack(array & $stack);
    public function getParams();
}
