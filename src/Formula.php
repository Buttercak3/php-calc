<?php
namespace Calc;

/**
 * Represents a mathematical formula that can be executed multiple times.
 */
class Formula
{
    /** @var Context */
    private $context;

    /** @var array */
    private $queue;

    /**
     * Executes a formula once.
     *
     * @param string $expression A mathematical expression
     * @param array $vars Temporary variables for this execution
     * @param Context $context A formula context
     * @return float
     */
    public static function exec($expression, $vars = [], Context $context = null)
    {
        $formula = new static($expression, $context);
        return $formula->execute($vars);
    }

    /**
     * Creates a new formula. An initial mathematical expression and a context object
     * can optionally be passed to initialize.
     *
     * @param string $expression A mathematical formula
     * @param Context $context A context object, otherwise Context::getDefault()
     */
    public function __construct($expression = null, Context $context = null)
    {
        if ($context) {
            $this->context = $context;
        } else {
            $this->context = Context::getDefault();
        }

        if ($expression !== null) {
            $this->parse($expression);
        }
    }

    /**
     * Executes the formula and returns the result. Temporary variables can be
     * passed as an accociative array.
     *
     * @param array $vars Key-value pairs of variables to be used during this execution
     * @return float
     */
    public function execute($vars = [])
    {
        foreach ($vars as $name => $value) {
            unset($vars[$name]);
            $name = strtolower($name);
            $vars[$name] = floatval($value);

        }

        $stack = [];

        foreach ($this->queue as $token) {
            if (is_numeric($token)) {
                array_push($stack, floatval($token));
            } elseif (isset($vars[$token])) {
                array_push($stack, $vars[$token]);
            } elseif ($this->context->isVar($token)) {
                array_push($stack, $this->context->getVar($token));
            } elseif ($this->context->isFunction($token)) {
                $func = $this->context->getFunction($token);
                $func->applyToStack($stack);
            } elseif ($this->context->isOperator($token)) {
                $op = $this->context->getOperator($token);
                $op->applyToStack($stack);
            } else {
                throw new FormulaExecutionError("Unknown token \"$token\"");
            }
        }

        if (count($stack) !== 1) {
            throw new FormulaExecutionError("Invalid syntax");
        }

        return $stack[0];
    }

    /**
     * Parses a new mathematical expression.
     *
     * @param string $expression
     * @return void
     */
    public function parse($expression)
    {
        $tokens = Tokenizer::parse($expression);
        $queue = [];
        $stack = [];
        $len = count($tokens);
        $prevToken = null;

        foreach ($tokens as $token) {
            if (is_numeric($token)) {
                array_push($queue, $token);
            } elseif ($this->context->isFunction($token)) {
                array_push($stack, $token);
            } elseif (is_identifier($token)) {
                array_push($queue, $token);
            } elseif ($this->context->isOperator($token)) {
                $this->pushOperator($token, $queue, $stack);
            } elseif ($token === "(") {
                if ($prevToken !== null && !$this->context->isOperator($prevToken) && !$this->context->isFunction($prevToken)) {
                    array_push($stack, "*");
                }
                array_push($stack, $token);
            } elseif ($token === ")") {
                $this->popOperators($queue, $stack);
            } elseif ($token === ",") {
                // TODO: Make this a function
                while (count($stack) && array_peek($stack) !== "(") {
                    array_push($queue, array_pop($stack));
                }

                if (count($stack) === 0) {
                    throw new ParseError("Mismatched parentheses");
                }
            } else {
                throw new ParseError("Unknown token \"$token\"");
            }

            $prevToken = $token;
        }

        while (count($stack) > 0) {
            $token = array_pop($stack);

            if ($token === "(" || $token === ")") {
                throw new ParseError("Mismatched parentheses");
            }

            array_push($queue, $token);
        }

        $this->queue = $queue;
    }

    private function pushOperator($symbol, array & $queue, array & $stack)
    {
        $op1 = $this->context->getOperator($symbol);

        while (count($stack) > 0 && $this->context->isOperator(array_peek($stack))) {
            $op2 = $this->context->getOperator(array_peek($stack));

            if (($op1->getAssociativity() === Operator::ASSOC_LEFT && $op1->getPrecedence() <= $op2->getPrecedence()) ||
                ($op1->getAssociativity() === Operator::ASSOC_RIGHT && $op1->getPrecedence() < $op2->getPrecedence())) {
                array_push($queue, array_pop($stack));
            } else {
                break;
            }
        }

        array_push($stack, $symbol);
    }

    private function popOperators(array & $queue, array & $stack)
    {
        while (count($stack) > 0 && array_peek($stack) !== "(") {
            array_push($queue, array_pop($stack));
        }

        if (count($stack) === 0) {
            throw new ParseError("Mismatched parentheses");
        }

        array_pop($stack);

        if (count($stack) > 0 && $this->context->isFunction(array_peek($stack))) {
            array_push($queue, array_pop($stack));
        }
    }

    public function getContext()
    {
        return $this->context;
    }
}
