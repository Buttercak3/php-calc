<?php
namespace Calc;

class Func implements IStackFunction
{
    /** @var string */
    private $name;

    /** @var int */
    private $params;

    /** @var callable */
    private $handler;

    /**
     * Creates a new function.
     *
     * @param string $name The name of the function
     * @param callable $handler A callback that resolves the function execution
     * @param int $params The number of parameters the function expects
     */
    public function __construct($name, callable $handler, $params = null)
    {
        $this->name = $name;
        $this->handler = $handler;

        if ($params !== null) {
            $this->params = $params;
        } else {
            $function = new \ReflectionFunction($handler);
            $this->params = $function->getNumberOfRequiredParameters();
        }
    }

    /**
     * Executes the callback with a set of arguments
     *
     * @param array $args The list of arguments to be passed to the callback
     * @return float
     */
    public function execute($args)
    {
        return call_user_func_array($this->handler, $args);
    }

    /**
     * Executes the function, but takes its parameters from the end of an array and pushes the result back into it.
     *
     * @param array $stack This will be modified
     * @return void
     */
    public function applyToStack(array & $stack)
    {
        $args = $this->params > 0 ? array_splice($stack, -$this->params) : [];
        $result = $this->execute($args);
        array_push($stack, $result);
    }

    /**
     * Returns the name of this function.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the required number of parameters for this function.
     */
    public function getParams()
    {
        return $this->params;
    }
}
